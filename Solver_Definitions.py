#!/usr/bin/env python
# -*- coding: utf-8 -*-

#============================================
#========== SAT SOLVER DEFINITIONS ==========
#============================================

#This file should include the definitions of functions used by the SAT solver.
import random
import copy

def solve_CNF(problem, guess_key="random",pure = True):
    """
    A safe version of solve_CNF_recursive
    A function used to determine if a logical formula is satisfiable using DPLL.

    Input:          problem;    a logical formula in CNF
                    gues_key;   determines the guessing algorithm
    Output:         set of variables that need to be set to True
    """
    temp_problem = copy.deepcopy(problem)
    if pure:
        solution = solve_CNF_recursive_pure(temp_problem, guess_key)
    else:
        solution = solve_CNF_recursive(temp_problem, guess_key)
    return solution

def solve_CNF_recursive(problem, guess_key):
    """
    A function used to determine if a logical formula is satisfiable using DPLL.

    Input:          problem;    a logical formula in CNF
                    gues_key;   determines the guessing algorithm
    Output:         set of variables that need to be set to True
    """

    #set of variables with value True
    variables_true = set()
    #set of variables with value False
    variables_false = set()
    i = 0

    clauses_to_remove = set()
    #Search for single literal conjunctions
    while i < len(problem):

        if len(problem[i]) == 1:
            single, = problem[i]
            if single in variables_true:
                #remove this clause
                clauses_to_remove.add(i)
                i+=1
            elif single in variables_false:
                #not solvable
                return None
            else:
                #found new single, add to sets
                variables_true.add(single)
                variables_false.add(-single)
                clauses_to_remove.add(i)
                i+=1
        else:
            #set clause to True
            if problem[i].intersection(variables_true) != set():
                clauses_to_remove.add(i)
                #move to the next
                i += 1
            else:
                #remove all variables that are False
                problem[i] = problem[i].difference(variables_false)
                if len(problem[i]) == 0:
                    #all are False
                    return None
                elif len(problem[i]) == 1:
                    single, = problem[i]
                    #found new single, add to sets
                    variables_true.add(single)
                    variables_false.add(-single)
                    clauses_to_remove.add(i)
                    i+=1
                else:
                    #move to the next
                    i += 1
        #cleanup
        if  i == len(problem) and clauses_to_remove != set():
            problem = [problem[j] for j in range(len(problem)) if j not in clauses_to_remove ]
            clauses_to_remove = set()
            #back to beginning
            i=0

    if len(problem) == 0:
        return variables_true

    # recursive
    guess = find_guess(problem, guess_key)
    # copy problem for guessing, add the guess as a unit-clause in the beginning
    guess_problem = [{guess}] + copy.deepcopy(problem)
    solution = solve_CNF_recursive(guess_problem, guess_key)
    #check if guess solved everything
    if solution is not None:
        variables_true.update(solution)
        return variables_true
    else:
        #guess again
        guess_problem = [{-guess}] + copy.deepcopy(problem)
        solution = solve_CNF_recursive(guess_problem, guess_key)
        if solution is not None:
            variables_true.update(solution)
            return variables_true
        else:
            return None

def find_guess(problem, guess_key):
    if guess_key=="frequent":
        frequency_dict = dict()
        for C in problem:
            for lit in C:
                frequency_dict[lit] = frequency_dict.get(lit, 0) + 1
        v=list(frequency_dict.values())
        k=list(frequency_dict.keys())
        return k[v.index(max(v))]
    elif guess_key=="random":
        temp = list(random.choice(problem))
        return random.choice(temp)

def test_solution(problem, solution):
    if solution == None:
        return None
    for clause in problem:
        if clause.intersection(solution) == set():
            return False
    return True

def solve_CNF_recursive_pure(problem, guess_key):
    """
    A function used to determine if a logical formula is satisfiable using DPLL.

    Input:          problem;    a logical formula in CNF
                    gues_key;   determines the guessing algorithm
    Output:         set of variables that need to be set to True
    """

    #set of variables with value True
    variables_true = set()
    #set of variables with value False
    variables_false = set()
    i = 0

    clauses_to_remove = set()
    #Search for single literal conjunctions
    while i < len(problem):

        if len(problem[i]) == 1:
            single, = problem[i]
            if single in variables_true:
                #remove this clause
                clauses_to_remove.add(i)
                i+=1
            elif single in variables_false:
                #not solvable
                return None
            else:
                #found new single, add to sets
                variables_true.add(single)
                variables_false.add(-single)
                clauses_to_remove.add(i)
                i+=1
        else:
            #set clause to True
            if problem[i].intersection(variables_true) != set():
                clauses_to_remove.add(i)
                #move to the next
                i += 1
            else:
                #remove all variables that are False
                problem[i] = problem[i].difference(variables_false)
                if len(problem[i]) == 0:
                    #all are False
                    return None
                elif len(problem[i]) == 1:
                    single, = problem[i]
                    #found new single, add to sets
                    variables_true.add(single)
                    variables_false.add(-single)
                    clauses_to_remove.add(i)
                    i+=1
                else:
                    #move to the next
                    i += 1
        #cleanup
        if  i == len(problem) and clauses_to_remove != set():
            problem = [problem[j] for j in range(len(problem)) if j not in clauses_to_remove ]
            clauses_to_remove = set()
            #back to beginning
            i=0
    if len(problem) == 0:
        return variables_true

    #finding pure variables:
    literals = set()
    for disj in problem:
        literals.update(disj)
    pure = set()
    for lit in literals:
        if (-lit) not in literals:
            pure.add(lit)
    for lit in pure:
        variables_true.add(lit)
        variables_false.add(-lit)
    #remove all clauses that contain pure literals
    clauses_to_remove = set()
    for j in range(len(problem)):
         if problem[j].intersection(pure) != set():
            clauses_to_remove.add(j)
    #cleanup
    if clauses_to_remove != set():
        problem = [problem[j] for j in range(len(problem)) if j not in clauses_to_remove ]
    if len(problem) == 0:
        return variables_true      

    # recursive
    guess = find_guess(problem, guess_key)
    # copy problem for guessing, add the guess as a unit-clause in the beginning
    guess_problem = [{guess}] + copy.deepcopy(problem)
    solution = solve_CNF_recursive_pure(guess_problem, guess_key)
    #check if guess solved everything
    if solution is not None:
        variables_true.update(solution)
        return variables_true
    else:
        #guess again
        guess_problem = [{-guess}] + copy.deepcopy(problem)
        solution = solve_CNF_recursive_pure(guess_problem, guess_key)
        if solution is not None:
            variables_true.update(solution)
            return variables_true
        else:
            return None

