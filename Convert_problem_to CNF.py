from Problems_to_CNF_Definitions import *

def vertex_k_coloring(graph, k=3, output = "", comment=""):
    """
    A function that converts the k-colorig of a graph question to a cnf fromula
    Input:          graph;     = (V,E), V = a list of vertices, E = a list of tuples (v1,v2), that are edges
                    k;         number of colors
                    output;    name of the output file
                    comment;   comment to be printed on a DIMACS format file
    Output:         Dimacs format file
    """
    solution = graph_coloring(graph,k)
    if output == "":
        output = "myGraph_in_DIMACS_cnf.txt"
    write_DIMACS(solution[0],solution[1],output,comment)
    return None

def sudoku_to_cnf(n,sudoku,clauses="all",output = "", comment=""):
    """
    Generate the CNF formula form a sudoku
    Input:                  n;    sudoku dimension n x n
                       sudoku;    a list that represents sparse matrix with given sudoku numbers e.g. [(1,5,9)] means there is 9 in row 1 col 5
                       clauses;   a parameter that says which clauses we use. Possible values: all, sq1, sq2
                       output;    name of the output file
                      comment;    comment to be printed on a DIMACS format file
    Output:              Dimacs format file
    """
    solution = sudoku_generate_CNF(n,sudoku,clauses)
    if output == "":
        output = "mySudoku_in_DIMACS_cnf.txt"
    write_DIMACS(solution[0],solution[1],output,comment)
    return None

    
###A few examples for testing:
##graph = ([1,2,3],[(1,2),(2,3),(1,3)])
##print("Making triangle. Graph = ", graph)
##vertex_k_coloring(graph,3,'triangle_3coloring.txt','CNF for 3-coloring the triangle')
##
##print("Making sudoku CNF")
##sparse_matrix = [(1,6,11),(1,7,4),(1,10,16),(1,14,8),
##                 (2,2,3),(2,3,12),(2,6,10),(2,11,4),(2,14,6),(2,15,2),
##                 (3,4,8),(3,5,16),(3,8,2),(3,9,9),(3,10,12),(3,12,6),(3,13,5),
##                 (4,1,1),(4,6,5),(4,7,7),(4,8,12),(4,10,10),(4,12,2),(4,15,9),
##                 (5,4,12),(5,6,6),(5,9,7),
##                 (6,1,16),(6,2,6),(6,4,1),(6,6,8),(6,9,15),(6,10,2),(6,12,14),(6,16,9),
##                 (7,2,7),(7,4,4),(7,5,10),(7,8,13),(7,11,12),(7,12,9),
##                 (8,7,1),(8,8,16),(8,11,3),(8,15,8),
##                 (9,2,15),(9,3,10),(9,5,6),(9,6,9),(9,7,5),(9,11,7),(9,12,8),(9,13,12),(9,15,3),
##                 (10,2,14),(10,7,8),(10,14,16),(10,15,4),(10,16,1),
##                 (11,3,8),(11,4,6),(11,5,3),(11,9,13),(11,10,15),(11,12,16),(11,14,2),
##                 (12,1,9),(12,2,5),(12,4,2),(12,11,1),(12,13,10),
##                 (13,3,3),(13,4,5),(13,10,4),(13,12,11),
##                 (14,3,14),(14,4,15),(14,5,1),(14,7,2),(14,10,9),
##                 (15,1,7),(15,8,14),(15,11,13),(15,12,15),(15,13,4),(15,14,3),(15,16,6),
##                 (16,3,13),(16,6,16),(16,9,1),(16,10,5),(16,13,16)]
##sudoku_to_cnf(16,sparse_matrix,"all",'custom_sudoku.txt', 'Custom made sudoku in CNF')



