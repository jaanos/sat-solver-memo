﻿# README #

This is the implementation of a DPLL algorithm in SAT-solver as a homework project for Logic in Computer Science course 
(Logika v računalništvu, 2016) at Faculty of Mathematics and Physics, University of Ljubljana.

### AUTHORS###

* Žiga Lukšič
* Anja Petković

### Contents ###

* The main program in SAT_Solver.py, which contains code for reading a problem in CNF from DIMACS format and the function solve, which solves the SAT problem
* Programs Solver_Definitions.py and Testing.py are supporting programs. Solver_Definitions contains all main functions used in function solve (mentioned above).
Testing.py consists of merely a few examples of testing the efficiency of calling functions with different parameters.
* Convert_problem_to_CNF.py contains two functions to translate problems to a CNF. One for k-colorig of a graph, another for sudoku problem. 
Problems_to_CNF.py is the supporting model
* testX.cnf are a few examples of CNF in Dimacs format used for testing 

### SAT-solver ###

* SAT-solver requires data in CNF (conjunctive normal form)
* Reading Dimacs format is implemented

### Usage ###

##### SAT_Solver - function 'solve' #####
To use the main 'solve' function use the following steps:

* Run python script SAT_Solver.py
* Run function solve with parameter input as the name of the file that contains SAT problem in CNF in Dimacs format. E.g. solve(myproblem.txt).

The function generates file input_solution.txt, where we can find the solution (values for variables). 
SOLUTION CONTAINS ONLY THE VARIABLES THAT NEED TO BE SET TO TRUE.
Additional parameters that can be used are: 

* *output*;         name of a .txt file where the solution should be saved. If left empty, a new file is generated.
* *output_save*;    if this parameter is set to False, the program will not save the solution in a file
* *guess_key*;      determines the guessing choice: either "random" or "frequent" - the most frequent literal in the conjunction. While the default is "random", "frequent" often outperforms it.
* *pure*;           if this parameter is set to False, the program will not use searching for pure variables

The function parameters that usually performed best on tests are solve(myproblem.txt, pure=True, guess_key="frequent").


##### Converting problems to CNF #####

To convert problems to cnf run the python file Convert_problem_to_CNF.py and run the desired function. 
For the function 'vertex_k_coloring' the input parameters are:

* graph;     = (V,E), V = a list of vertices, E = a list of tuples (v1,v2), that are edges
* k;         number of colors
* output;    name of the output file
* comment;   comment to be printed on a DIMACS format file

The function generates an output file (default is "myGraph_in_DIMACS_cnf.txt") that contains the problem converted to CNF in DIMACS format

For the function 'sudoku_to_cnf' the input parameters are:

* n;    sudoku dimension n x n, n needs to have a square root, that is a natural number
* sudoku;    a list that represents sparse matrix with given sudoku numbers e.g. [(1,5,9)] means there is a number 9 in row 1 and column 5
* clauses;   a parameter that says which clauses we use. Possible values: all, sq1, sq2
* output;    name of the output file
* comment;    comment to be printed on a DIMACS format file

The output is in the same form as the previously mentioned function.